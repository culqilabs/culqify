# Culqify

## Inicio rápido

Agrega esta línea a tu archivo .npmrc:

```text
@culqilabs:registry=https://gitlab.com/api/v4/packages/npm/
```

Obtener culqify con NPM:

```bash
npm install @culqilabs/culqify
```

Crear el archivo `server.ts` y agregar el siguiente contenido:

```typescript
import { Controller, Config, LoggerFactory, App } from '@culqilabs/culqify';

const controller = Controller.on('/', () => {
  return { hello: 'world' };
});

const config = Config.create();

const loggerFactory = LoggerFactory.create({ config });

const app = App.create({
  config,
  loggerFactory,
  controllers: [controller],
});

void app.start();

```

Finalmente, lanza el servidor con el comando

```bash
npm run once
```

puedes probarlo con ([REST Client][1]):

```text
POST http://localhost:3000/ HTTP/1.1
Content-Type: {{contentType}}

{
  "meta": {
    "serviceId": "{{serviceId}}",
    "timestamp": "{{$localDatetime rfc1123}}",
    "request": {
      "traceId": "{{$guid}}",
      "ipAddress": "{{hostname}}",
      "userId": "{{$guid}}"
    }
  },
  "data": {}
}
```

---
## Config

Permite obtener, establecer o eliminar una propiedad de un objeto anidado utilizando una ruta de puntos. Internamente hace uso de ([dot-prop][2]):

El método create, crea una instancia en base al argumento proporcionado o crea un objeto vacio si no se especifica.

Los métodos set, delete y merge mutan el objeto interno.

```typescript
import { Config } from '@culqilabs/culqify';

test('create', () => {
  const config = Config.create();
  config.set('a.b.c', 'hello');
  config.set('d.e.f', 1);
  config.merge({
    a: {
      b: {
        d: 'dummy',
      },
    },
    d: {
      e: {
        g: true,
      },
    },
  });

  expect(config.get('a.b.c')).toBe('hello');
  expect(config.get('a.b.d')).toBe('dummy');
  expect(config.get('d.e.f')).toBe(1);
  expect(config.get('d.e.g')).toBeTruthy();
  expect(config.get('g.h.i')).toBeUndefined();
});
```

Para crear una configuración en base a una plantilla, debe existir la ruta `config/template.ejs` en la raiz del proyecto ([ejs][2]).

```ejs
foo:
  a: <%= a %>
  b: <%= b %>
mix:
  <% mix.c.forEach((r) => { %>
  - <%= r %>
  <% }); %>

```

y llamar al método `createFromTemplate`, que recibe como primer parámetro la data a usar en la plantilla. El segundo parámetro es para cambiar los nombres de la carpeta y plantilla.

```typescript

test('createFromTemplate', () => {
  const config = Config.createFromTemplate({
    a: '1',
    b: '2',
    mix: {
      c: [1, 2, 3],
    },
  });

  expect(config.get('foo.a')).toBe(1);
  expect(config.get('foo.b')).toBe(2);
  expect(config.get('mix')).toEqual([1, 2, 3]);
});
```

---
## LoggerFactory

Un wrapper de Log4js ([log4js][4]).

La configuración de LoggerFactory debe encontrarse bajo la propiedad `loggerFactory` en config y seguir el siguiente esquema ([Joi][5]), si no se especifica se creará una configuración por defecto (Vease implementación).

```typescript
const schema = Joi.object<Config>({
  appenders: Joi.object()
    .pattern(
      /^/,
      Joi.object({
        type: Joi.string().required(),
      }).min(1),
    )
    .required()
    .min(1),
  categories: Joi.object()
    .pattern(
      /^/,
      Joi.object({
        appenders: Joi.array().items(Joi.string()),
        level: Joi.string().valid('trace', 'debug', 'info', 'warn', 'error', 'fatal'),
      }).min(1),
    )
    .required()
    .min(1),
});
```

Implementación:

```typescript
import { Config, LoggerFactory } from '@culqilabs/culqify';

// configuración por defecto
const plainConfig = {
  loggerFactory: {
    appenders: {
      out: {
        type: 'stdout',
      },
    },
    categories: {
      default: {
        appenders: ['out'],
        level: 'trace',
      },
    },
  },
};

const config = Config.create(plainConfig);

test('configure', () => {
  const loggerFactory = LoggerFactory.create({ config });

  // se especifica la categoria o por defecto es default
  const logger = loggerFactory.getLogger('default');

  logger.trace('trace');
  logger.debug('debug');
  logger.info('info');
  logger.warn('warn');
  logger.error('error');
  logger.fatal('fatal');

  expect(1).toBe(1);
});
```

---
## DataSource

Un wrapper de [mongoose][6], [ioredis][7] y [knex][8].

La configuración de DataSource debe encontrarse bajo la propiedad `dataSource` en config y seguir el siguiente esquema ([Joi][5]).

```typescript
const schema = Joi.object<Config>({
  mongoose: Joi.object()
    .pattern(
      /^/,
      Joi.object({
        uri: Joi.string().required(),
        useFindAndModify: Joi.boolean(),
        useCreateIndex: Joi.boolean(),
        useNewUrlParser: Joi.boolean(),
        useUnifiedTopology: Joi.boolean(),
        bufferCommands: Joi.boolean(),
        keepAlive: Joi.boolean(),
        keepAliveInitialDelay: Joi.number(),
        ssl: Joi.boolean(),
        sslValidate: Joi.boolean(),
        sslCA: Joi.array().items(Joi.string()),
      }),
    )
    .min(1),
  ioredis: Joi.object()
    .pattern(
      /^/,
      Joi.object({
        host: Joi.string().required(),
        port: Joi.number().required(),
      }),
    )
    .min(1),
  knex: Joi.object()
    .pattern(
      /^/,
      Joi.object({
        client: Joi.string().required(),
        connection: Joi.alternatives().try(Joi.object(), Joi.string()).required(),
      }).concat(Joi.object().pattern(/^/, Joi.any())),
    )
    .min(1),
});
```

Implementación:

```typescript
import pRetry from 'p-retry';
import delay from 'delay';
import { Config, LoggerFactory, HealthCheck, DataSource } from '@culqilabs/culqify';
import CatModel from './__helpers__/models/cat';

let dataSource: DataSource;

beforeAll(async () => {
  const plainConfig = {
    dataSource: {
      mongoose: {
        test: {
          uri: process.env.MONGO_URL,
        },
      },
    },
  };

  const config = Config.create(plainConfig);

  const loggerFactory = LoggerFactory.create({ config });

  const healthCheck = HealthCheck.create();

  dataSource = DataSource.create({
    healthCheck,
    config,
    loggerFactory,
  });

  const run = async (): Promise<void> => {
    if (!healthCheck.isReady()) {
      await delay(200);

      throw new Error('not ready');
    }
  };

  await pRetry(run, { retries: 5 });
});

afterAll(async () => {
  await dataSource.mongoose.test.close();
});

test('DataSource', async () => {
  const Cat = CatModel(dataSource.mongoose.test);

  await Cat.findByIdAndDelete('123456');

  await Cat.create({
    _id: '123456',
    name: 'Michi',
  });

  const found = await Cat.findById('123456');

  expect(found?.name).toBe('Michi');

  const michis = await Cat.find({});

  expect(michis.length).toBe(1);
});
```

---
## Commons

Módulo que permite acceder al dataSource `commons` y maneja operaciones sencillas sobre sus colecciones como insertOne, insertMany, find y findOne.

```typescript
import pRetry from 'p-retry';
import delay from 'delay';
import { Commons, Config, LoggerFactory, HealthCheck, DataSource } from '@culqilabs/culqify';

let dataSource: DataSource;

beforeAll(async () => {
  const plainConfig = {
    dataSource: {
      mongoose: {
        commons: {
          uri: process.env.MONGO_URL,
        },
      },
    },
  };

  const config = Config.create(plainConfig);

  const loggerFactory = LoggerFactory.create({ config });

  const healthCheck = HealthCheck.create();

  dataSource = DataSource.create({
    healthCheck,
    config,
    loggerFactory,
  });

  const run = async (): Promise<void> => {
    if (!healthCheck.isReady()) {
      await delay(200);

      throw new Error('not ready');
    }
  };

  await pRetry(run, { retries: 5 });
});

afterAll(async () => {
  await dataSource.mongoose.commons.close();
});

test('Commons', async () => {
  const commons = Commons.create({ dataSource });

  await commons.insertMany('foo', [
    { alias: 'a', slug: 'g1' },
    { alias: 'b', slug: 'g1' },
  ]);
  await commons.insertOne('foo', { alias: 'c', slug: 'g2' });

  const many = await commons.find('foo', { slug: 'g1' });

  expect(many).toHaveLength(2);

  const only = await commons.findOne<{ alias: string; slug: string }>('foo', { alias: 'c' });

  expect(only?.slug).toBe('g2');
});
```

---
## HealthCheck

Módulo que permite reportar el estado de los demás módulos.

---
## Queue

Módulo que permite la interactuar con Kafka o otro servicio de colas.

La configuración de Queue debe encontrarse bajo la propiedad `queue` en config y seguir el siguiente esquema ([Joi][5]).

```typescript
const schema = Joi.object<Config>()
  .pattern(
    /^/,
    Joi.object({
      brokers: Joi.array().items(Joi.string()).min(1).required(),
      ssl: Joi.boolean().default(false),
      producers: Joi.array()
        .items(
          Joi.object({
            topic: Joi.string().required(),
            timeout: Joi.number().default(30000),
          }),
        )
        .default([]),
      consumers: Joi.array()
        .items(
          Joi.object({
            topic: Joi.string().required(),
            fromBeginning: Joi.boolean().default(false),
          }),
        )
        .default([]),
    }),
  )
  .min(1);
```

---
## Remote

Módulo que interactua con SecretsManager de AWS, para obtener valores de acuerdo a configuración.

Para usarlo en local se necesita `localstack`.

Levantarlo con `docker-compose`

docker-compose.yml

```yaml
version: "3.8"

services:
  localstack:
    container_name: "${LOCALSTACK_DOCKER_NAME-localstack_main}"
    image: localstack/localstack
    network_mode: bridge
    ports:
      - "4566:4566"
      - "4571:4571"
      - "${PORT_WEB_UI-8080}:${PORT_WEB_UI-8080}"
    environment:
      - SERVICES=${SERVICES- }
      - DEBUG=${DEBUG- }
      - DATA_DIR=${DATA_DIR- }
      - PORT_WEB_UI=${PORT_WEB_UI- }
      - LAMBDA_EXECUTOR=${LAMBDA_EXECUTOR- }
      - KINESIS_ERROR_PROBABILITY=${KINESIS_ERROR_PROBABILITY- }
      - DOCKER_HOST=unix:///var/run/docker.sock
      - HOST_TMP_FOLDER=${TMPDIR}
    volumes:
      - "${TMPDIR:-/tmp/localstack}:/tmp/localstack"
      - "/var/run/docker.sock:/var/run/docker.sock"
```
Configurar el alias `awslocal` en tu archivo .bashrc o .zshrc (o el que sea).

```bash
alias awslocal="aws --endpoint-url=http://localhost:4566 --profile=local"
```

Comandos para interactuar con SecretsManager.

Se registran foo, bar y mix:

```bash
awslocal secretsmanager create-secret --name foo  --secret-string '{"a":"1"}'
awslocal secretsmanager create-secret --name bar  --secret-string '{"b":"2"}'
awslocal secretsmanager create-secret --name mix  --secret-string '{"c": [1, 2, 3]}'

awslocal secretsmanager get-secret-value --secret-id foo

awslocal secretsmanager update-secret --secret-id foo --secret-string '{"a":"1"}'
awslocal secretsmanager update-secret --secret-id bar --secret-string '{"b":"2"}'
```

En la raiz del proyecto crear un archivo `.culqifyrc.yml` con la siguiente definición:

```yml
secrets:
  foo:
    namespaced: false
  bar:
    namespaced: false
  mix:
    namespaced: true
```

`namespaced` igual a `false` quiere decir que remote cuando obtenga los valores de ese secreto los colocará en la raiz del objeto que generará, de lo contrario si se pone `true` se creará una propiedad con el nombre del secreto y su contenido irá en ese

Implementación:

```typescript
import { Remote, Config } from '@culqilabs/culqify';


test('Remote', async () => {
  const object = await Remote.loadSecrets({ env: 'local' });

  const config = Config.create(object);

  expect(config.get('a')).toBe('1');
  expect(config.get('b')).toBe('2');
  expect(config.get('mix.c')).toEqual([1, 2, 3]);
});
```

[1]: https://marketplace.visualstudio.com/items?itemName=humao.rest-client
[2]: https://www.npmjs.com/package/dot-prop
[3]: https://www.npmjs.com/package/ejs
[4]: https://www.npmjs.com/package/log4js
[5]: https://www.npmjs.com/package/joi
[6]: https://www.npmjs.com/package/mongoose
[7]: https://www.npmjs.com/package/ioredis
[8]: https://www.npmjs.com/package/knex